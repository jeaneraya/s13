// Creating Own Variables
let firstName = "Jean Victoria";
let lastName = "Eraya";
let age = 27;
let hobbies = ["Coding","Watching Netflix","Crocheting"];
let workAddress = {
		houseNumber: "Ground Floor, Fajardo Bldg.",
		streetName: "Jose Abad Santos St.",
		city: "Kidapawan City",
		state: "North Cotabato"
};

console.log("My firstname is " + firstName);

console.log("My lastname is " + lastName);

console.log("I'm currently " + age + " years old");

console.log("My hobies are: ");
console.log(hobbies);

console.log("I'm employed at ");
console.log(workAddress);


// Debugging

let fullName = "Steve Rogers";
	console.log("My full name is" + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestfriendName = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriendName);

	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);